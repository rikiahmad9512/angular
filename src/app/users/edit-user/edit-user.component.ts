import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  editUserForm : FormGroup = new FormGroup({});
  userId : String = '';
  userDetails : any;
  dataLoaded : boolean = false;
  
  constructor(private formBuilder : FormBuilder,
    private userService : UserService, private _snackBar: MatSnackBar, private activatedRouter : ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRouter.params.subscribe(data => {
      this.userId = data['id'];
    })

    this.userService.viewUser(this.userId).toPromise().then(data => {
      this.userDetails = data;
      Object.assign(this.userDetails, data);

      //Build Edit Form
      this.editUserForm = this.formBuilder.group({
        'name' : new FormControl(this.userDetails.name),
        'email'    : new FormControl(this.userDetails.email),
        'phone'    : new FormControl(this.userDetails.phone)
      })

      this.dataLoaded = true;
      console.log(this.userDetails);
    }).catch(err => {
      console.log(err);
    })
  }

  updateUser()
  {
    this.userService.updateUser(this.editUserForm.value, this.userId).subscribe(data => {
      this._snackBar.open("User Has Been Update");
      console.log(data);
    }, err =>{
      this._snackBar.open("Unable To Update User");
    })
  }

}
